$j(document).ready(function () {

    if (Modernizr.mq('only all and (min-width: 530px)')) { //max-width: 1025px
        var viewDay = 9;
    } else {

        var viewDay = 3;
        $j(".shop-calendar").insertBefore( ".post-list" );
    }

    $j.fn.wrapMatch = function(count) {

        var length = this.unwrap().length;
        var empty = viewDay - (length - (viewDay * (Math.floor(length / viewDay))));

        for(var i = 0; i < length ; i+=count) {
            this.slice(i, i+count).wrapAll('<li class="slide"></li>');
        }

        if (empty < viewDay) {
            var row = $j("<div class='day empty'></div>");
            for(var i = 0; i < empty; i++ )
                $j("li.slide:last-child").append(row.clone())
        }
        return this;
    };

    $j('.day').wrapMatch(viewDay);

    $j('.shop-calendar').flexslider({
        animationLoop: true,
        touch: true,
        controlsContainer: ".shop-calendar-title",
        controlNav: false
    });

});