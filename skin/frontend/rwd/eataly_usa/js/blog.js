/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     rwd_default
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

$j(document).ready(function() {

    if (Modernizr.mq('only all and (max-width: 1023px)')) {
        $j(".shop-events").appendTo(".col-main");
        $j(".shop-calendar").appendTo(".col-main");
        $j(".shops-list").insertAfter(".mondo-eataly-slider");
        $j(".smeraldo .shop-calendar").insertAfter(".events-list");
        $j(".smeraldo .shop-details").insertAfter(".page-title");
    }

    $j( ".trigger" ).click(function() {
        $j( ".drop" ).toggle( "fast");
        $j(this).toggleClass("open");
    });

    //Slider pagina negozi
    $j('.shop-post-slider').flexslider({
        animation: "slide",
        animationLoop: true,
        controlNav: true,
        directionNav: false
    });

    $j('.main-post-slider').flexslider({
        animation: "slide",
        keyboardNav: true
    });


    $j('.shop-post-slider .prev').on('click', function () {
        $j('.shop-post-slider').flexslider('prev');
        return false;
    })

    $j('.shop-post-slider .next').on('click', function () {
        $j('.shop-post-slider').flexslider('next');
        return false;
    })

    //slider prodotti associati
    $j('.associated-products').flexslider({
        animation: "slide",
        animationLoop: true,
        slideshow: false,
        itemWidth: 190,
        itemMargin: 18,
        minItems: 2,
        maxItems: 6,
        move: 1,
        controlNav: false
    });

    $j('.eatalyworld').flexslider({
        animation: "slide",
        animationLoop: true,
        touch: true,
        slideshow: false,
        itemWidth: 190,
        minItems: 2,
        maxItems: 6,
        move: 1,
        controlNav: true
    });


    $j('.flex-title-prev').on('click', function(){
        $j('.associated-products').flexslider('prev')
        return false;
    })

    $j('.flex-title-next').on('click', function(){
        $j('.associated-products').flexslider('next')
        return false;
    })

    //slider lista negozi del mondo
    $j('.world-shops').flexslider({
        animation: "slide",
        animationLoop: true,
        touch: true,
        itemWidth: 216,
        controlNav: false,
        minItems: 1,
        maxItems: 6,
        move: 1
    });

    //animazione box home
    var marginTopLIMore = 27;

    $j( ".post-list-item DIV.text" ).each(function( index ) {
        excerptHeight = $j(this).find("A.more").outerHeight() + marginTopLIMore; // + margin
        textHeight = $j(this).outerHeight();
        $j(this).height(textHeight - excerptHeight);
        //$j(this).css("border"," 1px solid red");
    });
    //
    $j(".post-list-item" )
    .mouseenter(function() {

        if( !$j(this).hasClass("open") ) {
            ThisExcerptHeight = $j(this).find("A.more").outerHeight();
            $j(this).find(".text").animate({
                height: ThisExcerptHeight + $j(this).find(".text").outerHeight() + marginTopLIMore
            }, 200, function () {
                $j(this).parent().parent().addClass("open");
            });
          }
        })
    .mouseleave(function() {

          if ( $j(this).hasClass("open") ) {
                ThisExcerptHeight = $j(this).find("A.more").outerHeight();
                //ThisHeight = $j( this ).find(".text").outerHeight();
                $j(this).find( ".text" ).animate({
                    height:  $j(this).find(".text").outerHeight() - ThisExcerptHeight - marginTopLIMore
                }, 200, function() {
                    $j(this).parent().parent().removeClass("open");
                });
          }
        });

    //MENU NEGOZI

    $j('#menu-negozi').parent().addClass("slider-menu-container");

    $j( "#menu-negozi a" ).each(function() {
        var href = $j(this).attr("href");

        if ((href.substr(href.length - 1)) == '/') {
            var noSlash = href.substring(0, href.length - 1);
            var slug = noSlash.substr(noSlash.lastIndexOf('/') + 1);
        } else {
            var slug = href.substr(href.lastIndexOf('/') + 1);
        }
        $j(this).addClass(slug.substr( 0, slug.indexOf('-')))
    });

    $j('#menu-negozi').parent().flexslider({
        animation: "slide",
        minItems: 2,
        itemWidth: 165,
        animationLoop: true,
        maxItems: 6,
        touch: true,
        selector: "#menu-negozi > li",
        slideshow: false,
        controlsContainer: ".slider-menu-container",
        controlNav: true
    });

    //ACCADE IN NEGOZIO

    $j('.shop-calendar').flexslider({
        animation: "slide",
        minItems: 2,
        itemWidth: 142,
        animationLoop: true,
        maxItems: 6,
        touch: true,
        selector: ".shop-calendar > ul",
        //slideshow: false,
        controlsContainer: ".shop-calendar-title",
        controlNav: false
    });

    // magazine sort posts by Recipe Course
    /*
     $j("#magazine-sort").change(function (){
     currentUrl = this.options[this.selectedIndex].baseURI;
     sortType = currentUrl.split('=');

     if (sortType.length == 1) {
     redirectUrl = sortType[0]+ "?sort=" + this.options[this.selectedIndex].value;
     } else {
     redirectUrl = sortType[0]+ "=" + this.options[this.selectedIndex].value;
     }

     window.location = redirectUrl;
     });
     */
});





