<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Helper_Data
    extends Mage_Core_Helper_Abstract
{

    /**
     * configuration for setting shop and magazine category id
     */
    const XML_PATH_WP_CATEGORY_MAGAZINE_ID = "wordpress/wordpress_integration/category_magazine_id";
    const XML_PATH_WP_CATEGORY_SHOP_ID = "wordpress/wordpress_integration/category_shop_id";
    const XML_PATH_WP_CATEGORY_ARCHIVE_SLUG_PREFIX = "wordpress/wordpress_integration/category_archive_slug_prefix";
    const XML_PATH_WP_CATEGORY_EATALY_WORLD_SLUG = "wordpress/wordpress_integration/category_eataly_world_slug";
    /**
     * custom layout handles for managing SHOP and MAGAZINE layout
     */
    const XML_LAYOUT_HANDLE_WP_CATEGORY_SHOP_LIST = "wordpress_category_shops_list";
    const XML_LAYOUT_HANDLE_WP_CATEGORY_SHOP_DETAIL = "wordpress_category_shops_detail";
    const XML_LAYOUT_HANDLE_WP_CATEGORY_SHOP_ARCHIVE = "wordpress_category_shops_archive";
    const XML_LAYOUT_HANDLE_WP_CATEGORY_SHOP_POST_VIEW = "wordpress_category_shops_post_view";
    const XML_LAYOUT_HANDLE_WP_CATEGORY_MAGAZINE_LIST = "wordpress_category_magazine_list";
    const XML_LAYOUT_HANDLE_WP_CATEGORY_MAGAZINE_DETAIL = "wordpress_category_magazine_detail";
    const XML_LAYOUT_HANDLE_WP_CATEGORY_MAGAZINE_ARCHIVE = "wordpress_category_magazine_archive";
    const XML_LAYOUT_HANDLE_WP_CATEGORY_MAGAZINE_POST_VIEW = "wordpress_category_magazine_post_view";

    /**
     * wp shop category names used to compose shop page detail
     */
    const SHOP_PAGE_DETAIL_SLIDER_CATEGORY_NAME = "slider-";
    const SHOP_PAGE_DETAIL_SHOWCASE_CATEGORY_NAME = "showcase-";
    const SHOP_PAGE_DETAIL_ARCHIVE_CATEGORY_NAME = "archive-";
    const SHOP_PAGE_DETAIL_MAGAZINE_CATEGORY_NAME = "widget-";

    /**
     * category attributes name for shop information
     */
    const SHOP_CATEGORY_ATTRIBUTE_TIMETABLE = "shop_timetable";
    const SHOP_CATEGORY_ATTRIBUTE_ADDRESS = "shop_address";
    const SHOP_CATEGORY_ATTRIBUTE_EMAIL = "shop_email";
    const SHOP_CATEGORY_ATTRIBUTE_PHONE = "shop_phone";
    const SHOP_CATEGORY_ATTRIBUTE_WIDGET_FB = "shop_widget_fb";
    const SHOP_CATEGORY_ATTRIBUTE_EVENTS_CATEGORY_ID = "shop_events_category_id";
    const SHOP_CATEGORY_OPTIONS_ATTRIBUTE_PREFIX = "taxonomy_";

    const MAGENTO_PRODUCT_ASSOCIATION_TO_WP_CATEGORY = "product/category";

    /**
     * wordpress attribute code used on SHOP POSTS
     */
    const WP_SHOP_SLIDER_CUSTOM_FIELD_TITLE = "wpcf-shop-slider-title";
    const WP_SHOP_SLIDER_CUSTOM_FIELD_SUBTITLE = "wpcf-shop-slider-titlesubtitle";
    const WP_SHOP_SLIDER_CUSTOM_FIELD_URL = "wpcf-shop-slider-url";

    const WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE = "wpcf-shop-calendar-date";
    const WP_SHOP_RESTAURANT_CUSTOM_FIELD_IMAGE = "wpcf-shop-restaurant-image";

    /**
     * parameter used for showing multiple events on same date
     */
    const SHOP_EVENT_CALENDAR_DATE = "event-date";

    /**
     * Events data EATALY SMERALDO
     */
    const WP_SHOPS_EVENTS_DATA =   "wpcf-eatalyspettacoli-data";
    const WP_SHOPS_EVENTS_TYPE =   "wpcf-eatalyspettacoli-genere";
    const WP_SHOPS_EVENTS_ARTIST = "wpcf-eatalyspettacoli-artista";

    /**
     * wordpress attribute code used for RECIPE posts
     */
    const WP_MAGAZINE_RECIPE_CUSTOM_FIELD_COURSE = "wpcf-magazine-recipe-course";
    const WP_MAGAZINE_RECIPE_CUSTOM_FIELD_DIFFICULTY = "wpcf-magazine-recipe-difficulty";
    const WP_MAGAZINE_RECIPE_CUSTOM_FIELD_PREPARING = "wpcf-magazine-recipe-preparing";

    /**
     * EATALY WORLD
     *
     * wordpress attribute code used for managing eatalyworld showcase posts
     */

    const XML_LAYOUT_HANDLE_WP_EATALY_WORLD = "wordpress_eataly_world";

    const WP_EATALYWORLD_CUSTOM_FIELD_POSTS_SHOWCASE = "wpcf-eatalyworld-showcase";
    const WP_EATALYWORLD_CUSTOM_FIELD_POSTS_SLIDER = "wpcf-eatalyworld-slider";
    const WP_EATALYWORLD_CUSTOM_FIELD_IMAGE_SLIDER = "wpcf-eatalyworld-slider-image";
    const WP_EATALYWORLD_CUSTOM_FIELD_IMAGE_SHOWCASE = "wpcf-eatalyworld-showcase-image";

    /**
     * wordpress attribute code used for managing MAGAZINE sidebar widget
     */
    const WP_MAGAZINE_CUSTOM_FIELD_WIDGET_RECENT = "wpcf-magazine-widget-recent";
    const WP_MAGAZINE_CUSTOM_FIELD_WIDGET_POPULAR = "wpcf-magazine-widget-popular";
    const WP_MAGAZINE_CUSTOM_FIELD_WIDGET_RANDOM = "wpcf-magazine-widget-random";

    /**
     * wordpress data from PLUGIN TYPES
     */
    const WP_PLUGIN_TYPES_ATTRIBUTE_CODE = "wpcf-fields";
    const WP_PLUGIN_TYPES_ATTRIBUTE_PREFIX = "wpcf-";
    const WP_PLUGIN_TYPES_ATTRIBUTE_OPTION_CODE = "wpcf-fields-select-option";

    const MAGAZINE_CATEGORY_SORT_ATTRIBUTE_CODE = "sort";
    const MAGAZINE_CATEGORY_SORT_ATTRIBUTE_DATE = "date";

    /**
     * Get Post/Events for Eataly Smeraldo
     *
     * @param $postId
     *
     * @return array
     */
    public function getShopEventsData($postId) {
        $postCustomData = Mage::getResourceModel("bitbull_cms/post_meta_collection")
            ->addFieldToFilter("meta_key", array(
                array("eq" => self::WP_SHOPS_EVENTS_DATA),
                array("eq" => self::WP_SHOPS_EVENTS_TYPE),
                array("eq" => self::WP_SHOPS_EVENTS_ARTIST)
            ))
            ->addFieldToFilter("post_id", array("eq" => $postId));

        $result = array();
        foreach ($postCustomData as $data) {
            $result[$data->getMetaKey()] = $data->getData("meta_value");
        }

        return $result;
    }

    public function getPostEventCustomData($postId)
    {
        $eventData = Mage::getResourceModel("bitbull_cms/post_meta_collection")
            ->addFieldToFilter("meta_key", array(
                array("eq" => self::WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE),

            ))
          ->addFieldToFilter("post_id", array("eq" => $postId));

        $result = array();
        foreach ($eventData as $data) {
            $result[$data->getMetaKey()] = $data->getData("meta_value");
        }

        return $result;
    }



    /**
     * Get showcase category posts associated with current shop
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    public function getShowcasePosts()
    {
        // get slider category posts
        // for category is used the following naming convension: slider_{category-name}

        /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
        $postCollection = Mage::getResourceModel('wordpress/post_collection');

        $postCollection->addIsPublishedFilter();

        /** @var $category Fishpig_Wordpress_Model_Post_Category */
        $category = $this->_getCurrentCategory();

        // @todo check if category is child of shop
        $currentSlug = self::SHOP_PAGE_DETAIL_SHOWCASE_CATEGORY_NAME . $category->getSlug();

        $postCollection->addCategorySlugFilter($currentSlug);

        return $postCollection;
    }


    /**
     * get current shop archive url
     *
     * @return string
     */
    public function getShopArchiveLink()
    {
        /** @var $category Fishpig_Wordpress_Model_Post_Category */
        $category = $this->_getCurrentCategory();

        $separator = (substr(Mage::helper('core/url')->getCurrentUrl(), -1, 1) != '/' ? '/' : '');

        return
            Mage::helper('core/url')->getCurrentUrl() . $separator . self::getShopPageArchiveCategorySlugPrefix()
            . $category->getSlug();
    }


    /**
     * get current wp category
     *
     * @return Fishpig_Wordpress_Model_Post_Category
     */
    protected function _getCurrentCategory()
    {
        return Mage::registry('wordpress_category');
    }


    /**
     * get wp category custom fields
     * the fields are saved on wp options table
     *
     * @param Fishpig_Wordpress_Model_Post_Category $category
     *
     * @return array mixed
     */
    public function getWpCategoryData($category)
    {
        $wpShopCategoryId = $this->getWpShopCategoryId();

        $categoryId = $this->getCurrentCategoryParentShopCategoryId($category);
        if ($categoryId == null || $categoryId == $wpShopCategoryId) {
            return $this;
        }

        $optionName = self::SHOP_CATEGORY_OPTIONS_ATTRIBUTE_PREFIX . $categoryId;
        $categoryOptions = Mage::helper("wordpress/abstract")->getPluginOption($optionName);

        return $categoryOptions;
    }

    /**
     *
     * @param $category
     */
    public function getCurrentCategoryParentShopCategoryId($category) {

        $wpShopCategoryId = $this->getWpShopCategoryId();

        if ($category->getId() == $wpShopCategoryId) {
            return $category->getId();
        } else if (null !== $category->getParent()) {
            if (false !== $category->getParentTerm()->getParentTerm()) {
                if ($category->getParentTerm()->getParent() == $wpShopCategoryId) {
                    return $category->getParentTerm()->getId();
                }
            } else {
                if ($category->getParent() == $wpShopCategoryId) {
                    return $category->getId();
                }
            }
        }

        return null;
    }

    /**
     * Get wp category shop id from backend configuration
     *
     * @return int mixed
     */
    public function getWpShopCategoryId()
    {
        return $this->_getWpCategoryShopId();
    }

    /**
     * Get wp category shop id from backend configuration
     *
     * @return int mixed
     */
    public function getWpMagazineCategoryId()
    {
        return $this->_getWpCategoryMagazineId();
    }



    /**
     * get shop parent category from post
     *
     * @param $post
     *
     * @return bool
     */
    public function getPostParentShopCategory($post)
    {
        if ($post->getParentCategory()->getParentTerm() !== false) {
            // check if category is child of SHOPS
            if ($post->getParentCategory()->getParentTerm()->getParent() !== null) {
                if ($post->getParentCategory()->getParentTerm()->getParent() == $this->getWpShopCategoryId()) {
                    return $post->getParentCategory()->getParentTerm();
                }
            }
        }
        return false;
    }


    /**
     * Retrieve the read DB adapter
     *
     * @return Varien_Db_Adapter_Interface
     */
    protected function _getReadAdapter()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_read');
    }


    /**
     * Retrieve a table name by entity
     *
     * @param string $entity
     * @return string
     */
    protected function _getTable($entity)
    {
        return Mage::getSingleton('core/resource')->getTableName($entity);
    }


    /**
     * Retrieve magento product information that are associated to the given wp category
     *
     * @param string   $type
     * @param int      $categoryId
     * @param int|null $storeId = null
     *
     * @return array
     */
    public function getAssociations($type = null, $categoryId = null, $storeId = null)
    {
        if (is_null($type)) {
            $type = self::MAGENTO_PRODUCT_ASSOCIATION_TO_WP_CATEGORY;
        }

        if (($typeId = Mage::helper("wordpress/associations")->getTypeId($type)) !== false) {
            if (is_null($storeId)) {
                $storeId = Mage::app()->getStore()->getId();
            }

            $category = Mage::registry("wordpress_category");
            if (null !== $category) {
                $categoryId = $this->_getParentShopCategoryId($category);
            } else if ($post = Mage::registry("wordpress_post")) {
                $categoryId = $post->getParentCategory()->getParent();
            }

            if ($categoryId == false) {
                return false;
            }

            // get magento products associated with given wp category
            $select = $this->_getReadAdapter()
                ->select()
                ->from($this->_getTable('wordpress/association'), array("object_id", 'position'))
                ->where('type_id=?', $typeId)
                ->where('wordpress_object_id=?', $categoryId)
                ->where('store_id=?', $storeId)
                ->order('position ASC');

            try {
                if (($results = $this->_getReadAdapter()->fetchAll($select)) !== false) {
                    $magentoProductIds = array();

                    foreach($results as $result) {
                        $magentoProductIds[] = $result["object_id"];
                    }
                    if (empty($magentoProductIds)) {
                        return false;
                    }
                    $associatedProductsCollection = $this->_getProductInfo($magentoProductIds);

                    return $associatedProductsCollection;
                }
            }
            catch (Exception $e) {
                Mage::log($e);
            }
        }

        return array();
    }


    /**
     * Get parent shop category from any sub categories
     *
     * @param Fishpig_Wordpress_Model_Post_Category $category
     *
     * @return $this|bool|mixed
     */
    protected function _getParentShopCategoryId($category)
    {
        if ($category == null) {
            return false;
        }

        $wpCategoryShopId = $this->_getWpCategoryShopId();

        if (!$category instanceof Fishpig_Wordpress_Model_Post_Category
            && $category->getTaxonomy() != "category"
        ) {
            return false;
        }

        // category level shop detail
        if (null !== $category->getParentCategory()->getId()) {
            if ($category->getParentCategory()->getId() == $wpCategoryShopId) {
                return $category->getId();
            }
        }

        // category level shop sub category
        if (null !== $category->getParentCategory()->getParentCategory()->getId()) {
            if ($category->getParentCategory()->getParentCategory()->getId() == $wpCategoryShopId) {
                return $category->getParentCategory()->getId();
            }
        }

        return false;
    }


    /**
     * Get wp category shop id from backend configuration
     *
     * @return int mixed
     */
    protected function _getWpCategoryShopId()
    {
        return Mage::getStoreConfig(self::XML_PATH_WP_CATEGORY_SHOP_ID);
    }


    /**
     * Get wp category MAGAZINE id from backend configuration
     *
     * @return int mixed
     */
    protected function _getWpCategoryMagazineId()
    {
        return Mage::getStoreConfig(self::XML_PATH_WP_CATEGORY_MAGAZINE_ID);
    }


    /**
     * Get magento products data
     *
     * @param $productIds
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function _getProductInfo($productIds)
    {
        if (!is_null($productIds)) {
            /* attributes to retrieve:
             * custom event date
             * name
             * price
             * image
             * product page url or add to cart url
             */
            $attributes = ["name", "final_price", "product_url", "small_image", "url_key", "price","event_date","event_start_time","event_end_time"];
            $collection =  Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect($attributes)
                ->addUrlRewrite()
                ->addAttributeToFilter('entity_id', $productIds);

            return $collection;
        }
        return $this;
    }


    /**
     * Get slider post custom data
     * post custom fields are save in wp table wp_postmeta
     *
     * @param $postId
     *
     * @return array
     */
    public function getSliderPostCustomData($postId)
    {
        $postCustomData = Mage::getResourceModel("bitbull_cms/post_meta_collection")
            ->addFieldToFilter("meta_key", array(
                    array("eq" => self::WP_SHOP_SLIDER_CUSTOM_FIELD_TITLE),
                    array("eq" => self::WP_SHOP_SLIDER_CUSTOM_FIELD_SUBTITLE),
                    array("eq" => self::WP_SHOP_SLIDER_CUSTOM_FIELD_URL)
                ))
            ->addFieldToFilter("post_id", array("eq" => $postId));

        $result = array();
        foreach ($postCustomData as $data) {
            $result[$data->getMetaKey()] = $data->getData("meta_value");
        }

        return $result;
    }


    /**
     * Get post custom field
     * post custom fields are save in wp table wp_postmeta
     *
     * @param $postId
     * @param $customFieldCode
     *
     * @return array
     */
    public function getPostCustomData($postId, $customFieldCode)
    {
        $postCustomData = Mage::getResourceModel("bitbull_cms/post_meta_collection")
            ->addFieldToFilter("meta_key", array(
                    array("eq" => $customFieldCode)
                ))
            ->addFieldToFilter("post_id", array("eq" => $postId));

        $result = array();
        foreach ($postCustomData as $data) {
            $result[$data->getMetaKey()] = $data->getData("meta_value");
        }

        return $result;
    }


    /**
     * Get shop category from shop subcategories
     *
     * @param $category
     *
     * @return $this|bool|mixed
     */
    public function getShopMainCategory($category)
    {
        return $this->_getParentShopCategory($category);
    }


    /**
     * Get parent shop category from any sub categories
     *
     * @param Fishpig_Wordpress_Model_Post_Category $category
     *
     * @return $this|bool|mixed
     */
    protected function _getParentShopCategory($category)
    {
        if ($category == null) {
            return false;
        }

        if (!$category instanceof Fishpig_Wordpress_Model_Post_Category
            && $category->getTaxonomy() != "category"
        ) {
            return false;
        }

        $wpCategoryShopId = $this->_getWpCategoryShopId();

        // category level shop detail
        if (null !== $category->getParentCategory()->getId()) {
            if ($category->getParentCategory()->getId() == $wpCategoryShopId) {
                return $category;
            }
        }

        // category level shop sub category
        if (null !== $category->getParentCategory()->getParentCategory()->getId()) {
            if ($category->getParentCategory()->getParentCategory()->getId() == $wpCategoryShopId) {
                return $category->getParentCategory();
            }
        }

        return false;
    }


    /**
     * Get data for event block found on shop details page
     *
     * @return array
     */
    public function getShopEventsCategoryData()
    {
        /** @var Fishpig_Wordpress_Model_Post_Category $category */
        $category = $this->_getCurrentCategory();

        // if no category found then extract parent category from post
        // if category found check if it is a shop main category, if not extract from parent category
        if (is_null($category)) {
            /** @var Fishpig_Wordpress_Model_Post $post */
            $post = Mage::registry("wordpress_post");

            $category = $this->getPostParentShopCategory($post);
        } else {
            $category = $this->getShopMainCategory($category);
        }

        // get wp category custom data
        $categoryData = $this->getWpCategoryData($category);

        // get magento category url for shop events
        // @todo check if this is a bottleneck, if so find a more efficient way to get category url from category id
        $categoryUrl = false;
        if (is_array($categoryData) && array_key_exists(self::SHOP_CATEGORY_ATTRIBUTE_EVENTS_CATEGORY_ID, $categoryData)) {
            $eventCategoryId = $categoryData[self::SHOP_CATEGORY_ATTRIBUTE_EVENTS_CATEGORY_ID];
            $categoryUrl = Mage::getModel("catalog/category")->load($eventCategoryId)->getUrl();
        }

        $categoryEventData = array(
            "category-name" => $category->getName(),
            "category-url"  => $categoryUrl ? $categoryUrl : null
        );

        return $categoryEventData;
    }


    /**
     * Get current category parent name
     *
     * @return string
     */
    public function getCategoryParentCategoryName()
    {
        return $this->_getCurrentCategory()->getParentCategory()->getName();
    }


    /**
     * Get Magazine category from given category hierarchy
     *
     * @param null|Fishpig_Wordpress_Model_Post_Category $category
     *
     * @return bool|Fishpig_Wordpress_Model_Post_Category
     */
    public function getMagazineMainCategory($category = null)
    {
        if (is_null($category)) {
            $category = $this->_getCurrentCategory();
        }

        if (!is_null($category)) {
            $wpCategoryMagazineId = $this->_getWpCategoryMagazineId();

            // category level magazine
            if (null !== $category->getId()) {
                if ($category->getId() == $wpCategoryMagazineId) {
                    return $category;
                }
            }

            // category level magazine sub category
            if (null !== $category->getParentCategory()->getId()) {
                if ($category->getParentCategory()->getId() == $wpCategoryMagazineId) {
                    return $category->getParentCategory();
                }
            }
        }

        return false;
    }


    /**
     * Get recipe post custom data
     * post custom fields are save in wp table wp_postmeta
     *
     * @param $postId
     *
     * @return array
     */
    public function getRecipePostCustomData($postId)
    {
        $postCustomData = Mage::getResourceModel("bitbull_cms/post_meta_collection")
            ->addFieldToFilter("meta_key", array(
                    array("eq" => self::WP_MAGAZINE_RECIPE_CUSTOM_FIELD_COURSE),
                    array("eq" => self::WP_MAGAZINE_RECIPE_CUSTOM_FIELD_DIFFICULTY),
                    array("eq" => self::WP_MAGAZINE_RECIPE_CUSTOM_FIELD_PREPARING)
                ))
            ->addFieldToFilter("post_id", array("eq" => $postId));

        $result = array();
        foreach ($postCustomData as $data) {
            $result[$data->getMetaKey()] = $data->getData("meta_value");
        }

        return $result;
    }


    /**
     * Filter post collection by Magazine Recipe Course
     *
     * @param Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    public function orderMagazinePosts($postCollection)
    {
        // get filter item from query string
        $action = Mage::app()->getFrontController()->getAction();
        $sortParameterValue = $action->getRequest()->getParam(self::MAGAZINE_CATEGORY_SORT_ATTRIBUTE_CODE);

        $sortParameterValue = (str_replace("-", " ", $sortParameterValue));

        // filter collection by filter item (Recipe Course)
        if ($sortParameterValue !== self::MAGAZINE_CATEGORY_SORT_ATTRIBUTE_DATE
            && $sortParameterValue !== ""
        ) {
            $postCollection->addMetaFieldToFilter(self::WP_MAGAZINE_RECIPE_CUSTOM_FIELD_COURSE, $sortParameterValue);
        }

        return $postCollection;
    }


    /**
     * Get Recipe Course Attribute Options
     *
     * @return array
     */
    public function getRecipeCourseAttributes()
    {
        // get attribute options for Recipe Course
        $categoryOptions = Mage::helper("wordpress/abstract")->getPluginOption(
            self::WP_PLUGIN_TYPES_ATTRIBUTE_CODE
        );

        // get attribute code of Recipe Course
        $recipeCourseAttributeCode = str_replace(
            self::WP_PLUGIN_TYPES_ATTRIBUTE_PREFIX, "", self::WP_MAGAZINE_RECIPE_CUSTOM_FIELD_COURSE
        );

        // get attribute options
        $courseAttribute = $categoryOptions[$recipeCourseAttributeCode]["data"]["options"];
        $parameterPrefix = self::WP_PLUGIN_TYPES_ATTRIBUTE_OPTION_CODE;

        // add attribute type date (will be used on filtering)
        $courseAttributeOptions = array(
            self::MAGAZINE_CATEGORY_SORT_ATTRIBUTE_DATE => $this->__("Data di pubblicazione")
        );
        // filter attribute options only by WP backend inserted options
        foreach ($courseAttribute as $key => $value) {
            if (strpos($key, $parameterPrefix) === 0) {
                if ($value["value"] !== "0") {
                    $courseAttributeOptions[strtolower(str_replace(" ", "-", $value["value"]))] = $value["value"];
                }
            }
        }

        return $courseAttributeOptions;
    }


    /**
     * Get active sorting order on magazine pages
     *
     * @return string
     */
    public function getMagazineSortingOrder()
    {
        // get sorting order from query string
        $action = Mage::app()->getFrontController()->getAction();
        $sortParameterValue = $action->getRequest()->getParam(self::MAGAZINE_CATEGORY_SORT_ATTRIBUTE_CODE);

        if (!is_null($sortParameterValue)) {
            return $sortParameterValue;
        } else {
            return self::MAGAZINE_CATEGORY_SORT_ATTRIBUTE_DATE;
        }
    }

    /**
     * Get category magazine url
     *
     * @return mixed
     *
     */

    public function getMagazineUrl()
    {
        $magazineId = Mage::getStoreConfig(Bitbull_Cms_Helper_Data::XML_PATH_WP_CATEGORY_MAGAZINE_ID);
        $magazineUrl = Mage::getModel('wordpress/post_category')->load($magazineId)->getUrl();
        return $magazineUrl;
    }

    public static function getShopPageArchiveCategorySlugPrefix() {
        return (
                Mage::getStoreConfig(self::XML_PATH_WP_CATEGORY_ARCHIVE_SLUG_PREFIX) ?
                Mage::getStoreConfig(self::XML_PATH_WP_CATEGORY_ARCHIVE_SLUG_PREFIX) :
                self::SHOP_PAGE_DETAIL_ARCHIVE_CATEGORY_NAME
        );
    }

    /**
     * prepara la collection di posts collegati a sottocategorie di magazine
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    public function getMagazineSubcategoriesPostCollection() {

        $magazineId = Mage::getStoreConfig(self::XML_PATH_WP_CATEGORY_MAGAZINE_ID);
        $category = Mage::getModel('wordpress/post_category')->load($magazineId);
        $childrenCategories = $category->getChildrenCategories();

        if ($category instanceof Fishpig_Wordpress_Model_Post_Category
            && $category->getId() ==  $magazineId
            && $childrenCategories->count())
            {
                /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
                $postCollection = Mage::getResourceModel('wordpress/post_collection');

                $postCollection->addIsViewableFilter();

                $subcategories = array();
                foreach ($childrenCategories as $childrenCategory) {
                    $subcategories[] = $childrenCategory->getId();
                }

                $postCollection->addCategoryAndPostIdFilter(array(), $subcategories);

                $postCollection->setOrderByPostDate();

                return $postCollection;
            }

        return false;
    }
}