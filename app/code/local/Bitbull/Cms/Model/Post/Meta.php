<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Model_Post_Meta extends Fishpig_Wordpress_Model_Abstract
{
	public function _construct()
	{
		$this->_init('bitbull_cms/post_meta');
	}

    /**
     * Loads an option based on it's post id
     *
     * @param string $postId
     *
     * @return $this
     */
    public function loadByPostId($postId)
    {
        $this->load($postId, 'post_id');
        return $this;
    }
}
