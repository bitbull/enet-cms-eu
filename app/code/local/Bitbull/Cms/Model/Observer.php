<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Model_Observer
{

    /**
     * used for deciding blog custom layout handle for categories and post of section SHOP and MAGAZINE
     */
    const SHOP_LIST = "shop_list";
    const SHOP_DETAIL = "shop_detail";
    const SHOP_ARCHIVE = "shop_archive";
    const SHOP_EVENTS = "shop_events";
    const SHOP_POST_VIEW = "shop_post";

    const MAGAZINE_LIST = "magazine_list";
    const MAGAZINE_DETAIL = "magazine_detail";
    const MAGAZINE_ARCHIVE = "magazine_archive";
    const MAGAZINE_POST_VIEW = "magazine_post_view";

    const EATALY_WORLD = "eataly_world";


    /**
     * Adds custom handle to wp category SHOP and MAGAZINE
     *
     * @param  Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function controllerActionLayoutLoadBefore(Varien_Event_Observer $observer)
    {
        // Check if wp category is SHOP or first level child
        /* @var $update Mage_Core_Model_Layout_Update */
        $update = $observer->getEvent()->getLayout()->getUpdate();

        /**
         * add wp category shop handles
         */
        switch ($this->_isWpMainCategory()) {
            case self::SHOP_DETAIL:
                $update->addHandle(Bitbull_Cms_Helper_Data::XML_LAYOUT_HANDLE_WP_CATEGORY_SHOP_DETAIL);
                break;
            case self::SHOP_ARCHIVE:
                $update->addHandle(Bitbull_Cms_Helper_Data::XML_LAYOUT_HANDLE_WP_CATEGORY_SHOP_ARCHIVE);
                break;
            case self::SHOP_LIST:
                $update->addHandle(Bitbull_Cms_Helper_Data::XML_LAYOUT_HANDLE_WP_CATEGORY_SHOP_LIST);
                break;
            case self::SHOP_POST_VIEW:
                $update->addHandle(Bitbull_Cms_Helper_Data::XML_LAYOUT_HANDLE_WP_CATEGORY_SHOP_POST_VIEW);
                break;
            case self::MAGAZINE_LIST:
                $update->addHandle(Bitbull_Cms_Helper_Data::XML_LAYOUT_HANDLE_WP_CATEGORY_MAGAZINE_LIST);
                break;
            case self::MAGAZINE_DETAIL:
                $update->addHandle(Bitbull_Cms_Helper_Data::XML_LAYOUT_HANDLE_WP_CATEGORY_MAGAZINE_DETAIL);
                break;
            case self::EATALY_WORLD:
                $update->addHandle(Bitbull_Cms_Helper_Data::XML_LAYOUT_HANDLE_WP_EATALY_WORLD);
                break;
        }
        return $this;
    }


    /**
     * Check if wp category is SHOP or first level child
     *
     * @return string Category type: {shop, shop child or shop child's child}
     */
    protected function _isWpMainCategory()
    {
        // if is post then add custom layout
        $post = Mage::registry("wordpress_post");
        if (!is_null($post)) {
            if ($this->_manageShopPostLayout($post)) {
                return self::SHOP_POST_VIEW;
            }
        }

        /** @var $category Fishpig_Wordpress_Model_Post_Category */
        $category = Mage::registry('wordpress_category');
        if ($category == null) {
            return false;
        }

        if (!$category instanceof Fishpig_Wordpress_Model_Post_Category
            && $category->getTaxonomy() != "category"
        ) {
            return false;
        }

        $wpCategoryShopId = $this->_getWpCategoryShopId();
        $wpCategoryMagazineId = $this->_getWpCategoryMagazineId();
        $slug_category_eataly_world = (
        Mage::getStoreConfig(Bitbull_Cms_Helper_Data::XML_PATH_WP_CATEGORY_EATALY_WORLD_SLUG) ?
            Mage::getStoreConfig(Bitbull_Cms_Helper_Data::XML_PATH_WP_CATEGORY_EATALY_WORLD_SLUG) :
            'mondo-eataly'
        );

        // check if main parent category is SHOP or MAGAZINE or EATALY-WORLD
        if ($category->getParentCategory() == null) {
            if ($category->getId() == $wpCategoryShopId) {
                return self::SHOP_LIST;
            } else if ($category->getId() == $wpCategoryMagazineId) {
                return self::MAGAZINE_LIST;
            } else if ($category->getSlug() == $slug_category_eataly_world) {
                return self::EATALY_WORLD;
            }
        } else {
            if ($category->getParentCategory()->getId() == $wpCategoryShopId) {
                return self::SHOP_DETAIL;
            } else if ($category->getParentCategory()->getId() == $wpCategoryMagazineId) {
                return self::MAGAZINE_DETAIL;
            }
        }


        // SHOP or MAGAZINE 2 lvl sub category (archive)
        // TODO: da rifattorizzare escludendo dinamicamente delle categorie
        if ($category->getParentCategory() && ($category->getParentCategory()->getParentCategory()) && null !== $category->getParentCategory()->getParentCategory()->getId()) {
            if ($category->getParentCategory()->getParentCategory()->getId() == $wpCategoryShopId && $category->getSlug() != 'palco-smeraldo') {
                return self::SHOP_ARCHIVE;
            } else if ($category->getParentCategory()->getParentCategory()->getId() == $wpCategoryMagazineId) {
                return self::MAGAZINE_ARCHIVE;
            }
        }
        return false;
    }


    /**
     * Manage blog posts of category SHOPS adding a custom layout
     *
     * @param $post
     *
     * @return bool
     */
    protected function _manageShopPostLayout($post)
    {
        // check if post category is child of shops
        $postParentCategoryId = $post->getParentCategory()->getParent();
        if (false !== $postParentCategoryId) {
            // load shop category
            $mainShopCategory = Mage::getModel('wordpress/post_category')->load($postParentCategoryId);

            if (false !== $mainShopCategory->getParent()) {
                if ($mainShopCategory->getParent() == $this->_getWpCategoryShopId()) {
                    return true;
                }
            }
        }

        return false;
    }


    /**
     * Get wp category shop id from backend configuration
     *
     * @return int mixed
     */
    protected function _getWpCategoryShopId()
    {
        return Mage::getStoreConfig(Bitbull_Cms_Helper_Data::XML_PATH_WP_CATEGORY_SHOP_ID);
    }


    /**
     * Get wp category magazine id from backend configuration
     *
     * @return int mixed
     */
    protected function _getWpCategoryMagazineId()
    {
        return Mage::getStoreConfig(Bitbull_Cms_Helper_Data::XML_PATH_WP_CATEGORY_MAGAZINE_ID);
    }
}