<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Block_Wordpress_Sidebar_Widget_Posts
    extends Fishpig_Wordpress_Block_Sidebar_Widget_Posts
{

    /**
     * override method in order to filter post collection by Magazine category
     * and to filter widget by type: recent, popular and random
     *
     * Adds on cateogry/author ID filters
     *
     * @return Fishpig_Wordpress_Model_Mysql4_Post_Collection
     */
    protected function _getPostCollection()
    {
        if (is_null($this->_collection)) {

            if ($categoryId = $this->getCategoryId()) {
                if (strpos($categoryId, ',') !== false) {
                    $categoryId = explode(',', trim($categoryId, ','));
                }

                $collection = Mage::getResourceModel('wordpress/post_collection');
                $collection->addCategoryIdFilter($categoryId)
                    ->addIsViewableFilter()
                    ->setOrderByPostDate();
            } else {
                // show only MAGAZINE posts
                $collection = Mage::helper('bitbull_cms')->getMagazineSubcategoriesPostCollection();
            }

            $collection->setPageSize($this->getNumber())->setCurPage(1);

            // custom logic for showing sidebar widget post: recent, popular, random
            $widgetType = $this->getWidgetType();
            // get custom post attribute used for managing magazine sidebar posts
            if ($widgetType == "recent") {
                $collection->addMetaFieldToFilter(
                    Bitbull_Cms_Helper_Data::WP_MAGAZINE_CUSTOM_FIELD_WIDGET_RECENT, true
                );
            } else if ($widgetType == "popular") {
                $collection->addMetaFieldToFilter(
                    Bitbull_Cms_Helper_Data::WP_MAGAZINE_CUSTOM_FIELD_WIDGET_POPULAR, true
                );
            } else if ($widgetType == "random") {
                $collection->addMetaFieldToFilter(
                    Bitbull_Cms_Helper_Data::WP_MAGAZINE_CUSTOM_FIELD_WIDGET_RANDOM, true
                );
                // randomize collection
                $collection->getSelect()->order(new Zend_Db_Expr('RAND()'));
            }

            if ($authorId = $this->getAuthorId()) {
                $collection->addAuthorIdFilter($authorId);
            }

            if ($tag = $this->getTag()) {
                $collection->addTermFilter($tag, 'post_tag', 'name');
            }

            if ($postTypes = $this->getPostType()) {
                $collection->addPostTypeFilter(explode(',', $postTypes));
            }

            $this->_collection = $collection;
        }

        return $this->_collection;
    }

}