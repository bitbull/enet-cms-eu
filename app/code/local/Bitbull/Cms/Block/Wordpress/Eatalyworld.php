<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Block_Wordpress_Eatalyworld
    extends Mage_Core_Block_Template
{

    /**
     * Get Eataly World slider posts
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    public function getSliderPosts()
    {
        /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
        $postCollection = Mage::getResourceModel('wordpress/post_collection');

        $numPostsToShow = $this->getSliderPostsCount();

        $postCollection->addIsPublishedFilter()
            // add custom post attribute used for managing eataly world slider
            ->addMetaFieldToFilter(Bitbull_Cms_Helper_Data::WP_EATALYWORLD_CUSTOM_FIELD_POSTS_SLIDER, true)
            ->setOrder('post_date', 'desc')
            ->setPageSize($numPostsToShow);

        return $postCollection;
    }

    protected function _getMagazineCategoryId()
    {
        return Mage::helper("bitbull_cms/data")->getWpMagazineCategoryId();
    }


    /**
     * Get post custom data
     *
     * @param $postId
     *
     * @return array
     */
    public function getPostCustomData($postId)
    {
        return Mage::helper("bitbull_cms/data")->getSliderPostCustomData($postId);
    }


    /**
     * Get Eataly World showcase posts
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    public function getShowcasePosts()
    {
        /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
        $postCollection = Mage::getResourceModel('wordpress/post_collection');

        $numPostsToShow = $this->getSliderPostsCount();

        $postCollection->addIsPublishedFilter()
            // add custom post attribute used for managing eataly world showcase posts
            ->addMetaFieldToFilter(Bitbull_Cms_Helper_Data::WP_EATALYWORLD_CUSTOM_FIELD_POSTS_SHOWCASE, true)
            ->setOrder('post_date', 'desc')
            ->setPageSize($numPostsToShow);

        return $postCollection;
    }

}