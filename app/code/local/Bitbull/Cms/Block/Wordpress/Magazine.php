<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Irene Iaccio <irene.iaccio@bitbull.it>
 */
class Bitbull_Cms_Block_Wordpress_Magazine
    extends Mage_Core_Block_Template
{

    /**
     * get last Magazine post
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    public function getMagazinePosts()
    {
        $magazineId = Mage::getStoreConfig(Bitbull_Cms_Helper_Data::XML_PATH_WP_CATEGORY_MAGAZINE_ID);

        $category = Mage::getModel('wordpress/post_category')->load($magazineId);
        if ($category instanceof Fishpig_Wordpress_Model_Post_Category
            && $category->getId() ==  $magazineId
            && $category->getChildrenCategories()->count()) {

            /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
            $postCollection = Mage::helper('bitbull_cms')->getMagazineSubcategoriesPostCollection($category->getChildrenCategories());
            $postCollection->setPageSize(5);
            return $postCollection;
        }

        return false;

    }


    /**
     * Get wordpress menu to be rendered on magazine first and second level categories
     *
     * @return mixed
     */
    public function getMagazineMenu()
    {
        // get menu name from category slug
        // category can be Magazine or it's first level category
        $magazineCategorySlug = Mage::helper("bitbull_cms/data")->getMagazineMainCategory()->getSlug();

        return Mage::getSingleton('core/layout')
            ->createBlock('bitbull_cms/wordpress_menu')
            ->setMenuName($magazineCategorySlug)
            ->includeWrapper(true)
            ->setListId('magazine-main-menu');
    }


}