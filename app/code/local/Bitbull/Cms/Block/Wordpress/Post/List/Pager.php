<?php
/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
*/ 
class Bitbull_Cms_Block_Wordpress_Post_List_Pager extends Fishpig_Wordpress_Block_Post_List_Pager
{
    /**
     * Return the URL for a certain page of the collection
     *
     * @param array $params
     * @return string
     */
    public function getPagerUrl($params=array())
    {
        $pageVarName = $this->getPageVarName();

        $pagination = isset($params[$pageVarName])
            ? $pageVarName . '/' . $params[$pageVarName] . '/'
            : '';

        $currentUrl = rtrim($this->getUrl('*/*/*', array(
            '_current' => false,
            '_escape' => true,
            '_use_rewrite' => true,
            '_query' => array('___refresh' => null),
        )), '/');

        $querystring = array();
        $query = $this->getRequest()->getQuery();

        foreach ($query as $key => $value) {
            $querystring[] = $key . '=' . $value;
        }

        return $currentUrl . '/' . $pagination . (sizeof($querystring) > 0 ? '?' . implode('&', $querystring) : '');
    }
}