<?php
/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
*/ 
class Bitbull_Cms_Block_Wordpress_Post_List extends Fishpig_Wordpress_Block_Post_List
{
    /**
     * Generates and returns the collection of posts
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    protected function _getPostCollection()
    {
        if (is_null($this->_postCollection) && $this->getWrapperBlock()) {
            $this->_postCollection = $this->getWrapperBlock()->getPostCollection();

            if ($this->_postCollection) {

                // get event date from query string
                // if found then show date and filter posts that have an event set for the given date
                $eventDate = $this->getAction()->getRequest()->getParam(Bitbull_Cms_Helper_Data::SHOP_EVENT_CALENDAR_DATE);
                if ($eventDate) {
                    $this->_postCollection
                        ->addCustomFieldFilter(Bitbull_Cms_Helper_Data::WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE, strtotime($eventDate . ' 00:00:00'), '>=')
                        ->addCustomFieldFilter(Bitbull_Cms_Helper_Data::WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE, strtotime($eventDate . ' 23:59:59'), '<=')
                    ;
                }

                if ($this->getPostType()) {
                    $this->_postCollection->addPostTypeFilter($this->getPostType());
                }

                if ($this->getPagerBlock()) {
                    $this->getPagerBlock()->setCollection($this->_postCollection);
                }
            }
        }

        return $this->_postCollection;
    }

    public function getEventDateTitle()
    {
        $eventDate = $this->getAction()->getRequest()->getParam(Bitbull_Cms_Helper_Data::SHOP_EVENT_CALENDAR_DATE);

        if ($eventDate) {
            return date('F j', strtotime($eventDate));
        }

        return '';
    }

    public function getMagazinePosts()
    {
        /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
        $postCollection =  Mage::getResourceModel('wordpress/post_collection');

        /** @var $category Fishpig_Wordpress_Model_Post_Category */
        $category = Mage::registry('wordpress_category');

        // @todo check if category is child of shop
        $currentSlug = Bitbull_Cms_Helper_Data::SHOP_PAGE_DETAIL_MAGAZINE_CATEGORY_NAME. $category->getSlug();

        $postCollection->addCategorySlugFilter($currentSlug);


        // get event date from query string
        // if found then show date and filter posts that have an event set for the given date
        $eventDate = $this->getAction()->getRequest()->getParam(Bitbull_Cms_Helper_Data::SHOP_EVENT_CALENDAR_DATE);
        if ($eventDate) {
            $postCollection
                ->addCustomFieldFilter(Bitbull_Cms_Helper_Data::WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE, strtotime($eventDate . ' 00:00:00'), '>=')
                ->addCustomFieldFilter(Bitbull_Cms_Helper_Data::WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE, strtotime($eventDate . ' 23:59:59'), '<=');
        }

        if ($this->getPostType()) {
            $postCollection->addPostTypeFilter($this->getPostType());
        }

        if ($this->getPagerBlock()) {
            $this->getPagerBlock()->setCollection($postCollection);
        }
        return $postCollection;

    }

    public function getMagazineSubcategoriesPosts()
    {
        /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
        $postCollection = Mage::helper('bitbull_cms')->getMagazineSubcategoriesPostCollection();

        if ($postCollection instanceof Fishpig_Wordpress_Model_Resource_Post_Collection) {
            if ($this->getPostType()) {
                $postCollection->addPostTypeFilter($this->getPostType());
            }

            if ($this->getPagerBlock()) {
                $this->getPagerBlock()->setCollection($postCollection);
            }
        }

        return $postCollection;

    }

    /**
   	 * Returns the collection of posts
   	 *
   	 * @return Fishpig_Wordpress_Model_Mysql4_Post_Collection
   	 */
   	public function getPosts()
   	{
        if($this->getMagazineWidgetPosts()){
            return $this->getMagazinePosts();
        } else {

            // se sono nella pagina magazine estrazione dei posts collegati alle sottocategorie
            // per risolvere il problema delle immagini e degli urls
            $category = Mage::registry('wordpress_category');
            $magazineId = Mage::getStoreConfig(Bitbull_Cms_Helper_Data::XML_PATH_WP_CATEGORY_MAGAZINE_ID);
            if ($category instanceof Fishpig_Wordpress_Model_Post_Category
                && $category->getId() ==  $magazineId)
            {
                return $this->getMagazineSubcategoriesPosts();
            }
        }

        return parent::getPosts();

   	}

    public function getSpettacoliPosts() {
        $collection = parent::getPosts();
        $collection->joinCustomField(Bitbull_Cms_Helper_Data::WP_SHOPS_EVENTS_DATA, 'join')
                    ->getSelect()
                    ->order(Bitbull_Cms_Helper_Data::WP_SHOPS_EVENTS_DATA . ' desc');
        return $collection;
    }
}