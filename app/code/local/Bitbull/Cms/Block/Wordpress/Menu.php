<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Block_Wordpress_Menu
    extends Fishpig_Wordpress_Block_Menu
{

    /**
     * Load and render the menu
     *
     * rewrite block in order to have the possibility to load a wp menu by SLUG
     *
     * @return bool
     */
    protected function _beforeToHtml()
    {
        // load menu by id
        if ($this->getMenuId()) {
            $menu = Mage::getModel('wordpress/menu')->load($this->getMenuId());

            if ($menu->getId()) {
                $this->setMenu($menu);

                if ($menu->applyToTreeNode($this->_menu)) {
                    if (($html = trim($this->getHtml())) !== '') {
                        if ($this->includeWrapper()) {
                            $html = sprintf('<ul %s>%s</ul>', $this->_getListParams(), $html);
                        }

                        $this->setMenuHtml($this->_beforeRenderMenuHtml($html));
                    }

                    return true;
                }
            }
        }

        // load menu by slug
        if ($this->getMenuName()) {
            $menu = Mage::getModel('wordpress/menu')->load($this->getMenuName(), "slug");

            if ($menu->getId()) {
                $this->setMenu($menu);

                if ($menu->applyToTreeNode($this->_menu)) {
                    if (($html = trim($this->getHtml())) !== '') {
                        if ($this->includeWrapper()) {
                            $html = sprintf('<ul %s>%s</ul>', $this->_getListParams(), $html);
                        }

                        $this->setMenuHtml($this->_beforeRenderMenuHtml($html));
                    }

                    return true;
                }
            }

        }

        return false;
    }

    /**
     * Retrieve cache key data
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $cacheId = parent::getCacheKeyInfo();

        $cacheId['menu_id'] = $this->getMenuId();
        $cacheId['menu_name'] = $this->getMenuName();

        return $cacheId;
    }
}