<?php
/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Nadia Sala <nadia.sala@bitbull.it>
 */
class Bitbull_Cms_Block_UrlRewrite_Adminhtml_UrlRedirect_Grid extends Enterprise_UrlRewrite_Block_Adminhtml_UrlRedirect_Grid {

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('redirect_id');
        $this->setMassactionIdFilter('redirect_id');
        $this->setMassactionIdFieldOnlyIndexValue(true);
        $this->getMassactionBlock()->setFormFieldName('url_redirects');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('review')->__('Delete'),
            'url'  => $this->getUrl(
                '*/*/massDelete',
                array('ret' => Mage::registry('usePendingFilter') ? 'pending' : 'index')
            ),
            'confirm' => Mage::helper('bitbull_cms')->__('Are you sure?')
        ));
    }
}