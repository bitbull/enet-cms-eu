<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Block_Shops_Calendar
    extends Mage_Core_Block_Template
{

    /**
     * get post to be used on calendar on shop pages
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    public function getPostsWithDate()
    {
        /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
        $postCollection = Mage::getResourceModel('wordpress/post_collection');

        /** @var Fishpig_Wordpress_Model_Post_Category $category */
        $category = Mage::registry("wordpress_category");

        // if no category found then extract parent category from post
        // if category found check if it is a shop main category, if not extract from parent category
        if (is_null($category)) {
            /** @var Fishpig_Wordpress_Model_Post $post */
            $post = Mage::registry("wordpress_post");

            $category = Mage::helper("bitbull_cms/data")->getPostParentShopCategory($post);
        } else {
            $category = Mage::helper("bitbull_cms/data")->getShopMainCategory($category);
        }

        $currentSlug = $category->getSlug();
        $currentCategoryArchiveSlug = Bitbull_Cms_Helper_Data::getShopPageArchiveCategorySlugPrefix() . $currentSlug;

        // get post associated with given archive shop category
        $postCollection->addCategorySlugFilter($currentCategoryArchiveSlug);

        $posts = array();
        $date = new DateTime();
        $currentDate = Mage::getModel("core/date")->gmtDate("j-n-Y");
        $currentDateTimestamp = strtotime($currentDate);
        $currentDateObject = new DateTime();
        $currentDateObject->setTimestamp($currentDateTimestamp);

        foreach ($postCollection as $post) {
            // get event custom date from post
            $eventDate = $this->getPostEventCustomDate($post->getId());

            if (!empty($eventDate)
                && !empty($eventDate[Bitbull_Cms_Helper_Data::WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE])
            ) {
                // get date in full format containing week day, day number and month

                $date->setTimestamp($eventDate[Bitbull_Cms_Helper_Data::WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE]);
                $formatedDate = $date->format("l j n F Y");



                // full format es. "Monday 8 6 June 2015"
                $dateArray = explode(" ", $formatedDate);
                $eventDateWeekDay = $dateArray[0];
                $eventDateDay = $dateArray[1];
                $eventDateMonthInt = $dateArray[2];
                $eventDateMonth = $dateArray[3];
                $eventDateYear = $dateArray[4];

                $fullDate = $eventDateYear . "-" . $eventDateMonthInt . "-" . $eventDateDay  ;

                // show event only if event date is not in past from current day
                $interval = $currentDateObject->diff($date);
                $dateDiffInDays = $interval->format('%R%a');

                if ($dateDiffInDays >= 0) {
                    $posts[$fullDate][] = array(
                        "url" => $post->getUrl(),
                        "event-date-week-day" => $eventDateWeekDay,
                        "event-date-day" => $eventDateDay,
                        "event-date-month" => $eventDateMonth,
                        "event-full-date" => $fullDate
                    );
                }
            }
        }

        // sort events by date ASC
        ksort($posts, SORT_NATURAL | SORT_FLAG_CASE);

        return $posts;
    }


    /**
     * Get post custom data
     *
     * @param $postId
     *
     * @return array
     */
    public function getPostEventCustomDate($postId)
    {
        return Mage::helper("bitbull_cms/data")->getPostCustomData(
            $postId, Bitbull_Cms_Helper_Data::WP_SHOP_POST_EVENT_ATTRIBUTE_CUSTOM_DATE
        );
    }


    /**
     * Get custom url for calendar dates with multiple events
     *
     * @param $eventDate
     *
     * @return string
     */
    public function getMultipleCalendarEventsUrl($eventDate)
    {

        /** @var Fishpig_Wordpress_Model_Post_Category $category */
        $category = Mage::registry("wordpress_category");

        if (is_null($category)) {
            /** @var Fishpig_Wordpress_Model_Post $post */
            $post = Mage::registry("wordpress_post");

            $category = Mage::helper("bitbull_cms/data")->getPostParentShopCategory($post);
        } else {
            $category = Mage::helper("bitbull_cms/data")->getShopMainCategory($category);
        }

        // modify url to link to shop archive
        $categoryUrl = $category->getUrl();
        $postArchiveUrl
            = $categoryUrl . Bitbull_Cms_Helper_Data::getShopPageArchiveCategorySlugPrefix() . $category->getSlug();

        $urlParameters = array(Bitbull_Cms_Helper_Data::SHOP_EVENT_CALENDAR_DATE => $eventDate);
        return Mage::helper("core/url")->addRequestParam($postArchiveUrl, $urlParameters);
    }

}