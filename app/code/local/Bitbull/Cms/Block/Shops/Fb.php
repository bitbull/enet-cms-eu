<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Block_Shops_Fb
    extends Mage_Core_Block_Template
{


    /**
     * Get wp category custom filed - FB WIDGET
     *
     * @return string
     */
    public function getCategoryFbWidgetData()
    {
        /** @var Fishpig_Wordpress_Model_Post_Category $category */
        $category = Mage::registry("wordpress_category");

        if (is_null($category)) {
            /** @var Fishpig_Wordpress_Model_Post $post */
            $post = Mage::registry("wordpress_post");
            $category = Mage::helper("bitbull_cms/data")->getPostParentShopCategory($post);
        }

        $categoryCustomData = Mage::helper("bitbull_cms/data")->getWpCategoryData($category);

        $fbWidgetData = null;
        if (is_array($categoryCustomData) && array_key_exists(Bitbull_Cms_Helper_Data::SHOP_CATEGORY_ATTRIBUTE_WIDGET_FB, $categoryCustomData)) {
            $fbWidgetData = stripslashes($categoryCustomData[Bitbull_Cms_Helper_Data::SHOP_CATEGORY_ATTRIBUTE_WIDGET_FB]);
        }

        return $fbWidgetData;
    }

}