<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Irene Iaccio <irene.iaccio@bitbull.it>
 */
class Bitbull_Cms_Block_Shops_Partner
    extends Mage_Core_Block_Template
{
    /**
     * @return string
     */
    public function getCategoryPartner()
    {
        /** @var Fishpig_Wordpress_Model_Post_Category $category */
        $category = Mage::registry("wordpress_category");

        if (is_null($category)) {
            /** @var Fishpig_Wordpress_Model_Post $post */
            $post = Mage::registry("wordpress_post");
            $category = Mage::helper("bitbull_cms/data")->getPostParentShopCategory($post);
        }

        $html = '';
        $shopCategoryId = Mage::helper("bitbull_cms/data")->getCurrentCategoryParentShopCategoryId($category);
        if ($shopCategoryId) {
            $category = Mage::getModel('wordpress/post_category')->load($shopCategoryId);
            $url = $category->getUrl() . 'i-partner-di-' . $category->getSlug();
            $name = $this->__('Partners of ') . $category->getName();
            $html = '<a href=' . $url . '>' . $name . '<em class="icon-double-angle-right"></em></a>';
        }

        return $html;
    }

}