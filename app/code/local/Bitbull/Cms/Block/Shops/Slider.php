<?php

/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_Cms_Block_Shops_Slider
    extends Mage_Core_Block_Template
{

    /**
     * get post to be used on blog shop detail, slider
     *
     * @return Fishpig_Wordpress_Model_Resource_Post_Collection
     */
    public function getSliderPosts()
    {
        // get slider category posts
        // for category is used the following naming convension: slider_{category-name}

        /** @var Fishpig_Wordpress_Model_Resource_Post_Collection $postCollection */
        $postCollection =  Mage::getResourceModel('wordpress/post_collection');

        $postCollection->addIsPublishedFilter();

        /** @var $category Fishpig_Wordpress_Model_Post_Category */
        $category = Mage::registry('wordpress_category');

        // @todo check if category is child of shop
        $currentSlug = Bitbull_Cms_Helper_Data::SHOP_PAGE_DETAIL_SLIDER_CATEGORY_NAME . $category->getSlug();

        $postCollection->addCategorySlugFilter($currentSlug);

        return $postCollection;
    }


    /**
     * Get post custom data
     *
     * @param $postId
     *
     * @return array
     */
    public function getPostCustomData($postId)
    {
        return Mage::helper("bitbull_cms/data")->getSliderPostCustomData($postId);
    }

}