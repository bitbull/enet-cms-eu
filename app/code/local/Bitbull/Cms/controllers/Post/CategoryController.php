<?php
/**
 * @category Bitbull
 * @package  Bitbull_Cms
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */

require_once Mage::getModuleDir('controllers', 'Fishpig_Wordpress') . DS . 'Post' . DS . 'CategoryController.php';

class Bitbull_Cms_Post_CategoryController
    extends Fishpig_Wordpress_Post_CategoryController
{


    /**
     * rewrite in order to create layout handles based on category slug
     *
     * Display the category page and list blog posts
     *
     */
    public function viewAction()
    {
        $category = Mage::registry('wordpress_category');

        // create custom layout handle by slug instead by id
        $this->_addCustomLayoutHandles(
            array(
                'wordpress_post_category_view',
                'wordpress_category_' . $category->getSlug(),
                'wordpress_post_list',
                'wordpress_term',
            )
        );

        $this->_initLayout();

        $this->_rootTemplates[] = 'post_list';

        $tree = array($category);
        $buffer = $category;

        while (($buffer = $buffer->getParentCategory()) !== false) {
            array_unshift($tree, $buffer);
        }

        while (($branch = array_shift($tree)) !== null) {
            $this->addCrumb(
                'category_' . $branch->getId(), array(
                    'link'  => ($tree ? $branch->getUrl() : null),
                    'label' => $branch->getName())
            );

            $this->_title($branch->getName());
        }

        $this->renderLayout();
    }

}