<?php
 /**
 * Class     WorldeatalyController.php
 * @category Bitbull
 * @package  Bitbull_WorldEataly
 * @author   Mirko Cesaro <mirko.cesaro@gmail.com>
 */

class Bitbull_WorldEataly_WorldeatalyController extends Mage_Core_Controller_Front_Action{

    public function indexAction(){
        $this->loadLayout();
        $this->renderLayout();
    }
} 